import { AddNewsComponent } from './dashboard/admin/managenews/add-news/add-news.component';
import { DashboardmainComponent } from './dashboard/dashboardmain/dashboardmain.component';
import { ManagenewsComponent } from './dashboard/admin/managenews/managenews.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './dashboard/login/login.component';
import { NewsDetailsComponent } from './pages/news/news-details/news-details.component';
import { NewsListComponent } from './pages/news/news-list/news-list.component';
import { AttorneyListComponent } from './pages/attorneys/attorney-list/attorney-list.component';
import { AttorneydetailsComponent } from './pages/attorneys/attorneydetails/attorneydetails.component';
import { NewsComponent } from './pages/news/news.component';
import { ContactComponent } from './pages/contact/contact.component';
import { CasesComponent } from './pages/cases/cases.component';
import { ServicesComponent } from './pages/services/services.component';
import { HomeComponent } from './home/home.component';
import { PagesComponent } from './pages/pages.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirmComponent } from './pages/firm/firm.component';
import { AttorneysComponent } from './pages/attorneys/attorneys.component'
import { AdminComponent } from './dashboard/admin/admin.component';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'dashboard', component: DashboardComponent,
    children: [
      { path: '', component: LoginComponent },
      { path: 'admin', component: AdminComponent, children: [
        { path: '', component: DashboardmainComponent },
        { path: 'news', component: ManagenewsComponent },
        { path: 'addnews', component: AddNewsComponent }
      ]
      } ,
    ] },
  { path: 'page', component: PagesComponent
  , children: [
    { path: 'firm', component: FirmComponent },
    { path: 'attorneys', component: AttorneysComponent,
    children: [
      { path: '', component: AttorneyListComponent },
      { path: ':id', component: AttorneydetailsComponent }
    ] },
    { path: 'services', component: ServicesComponent },
    { path: 'cases', component: CasesComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'news', component: NewsComponent,
    children: [
      { path: '', component: NewsListComponent },
      { path: ':id', component: NewsDetailsComponent }
    ] }
  ]
 },
//  { path: 'attorney-details', component: AttorneyDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
