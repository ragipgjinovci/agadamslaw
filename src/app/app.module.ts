
import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import { HeaderComponent } from './pages/header/header.component';
import { PagesComponent } from './pages/pages.component';
import { FooterComponent } from './footer/footer.component';
import { FirmComponent } from './pages/firm/firm.component';
import { SidebarComponent } from './pages/sidebar/sidebar.component';
import { AttorneysComponent } from './pages/attorneys/attorneys.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { ServicesComponent } from './pages/services/services.component';
import { CasesComponent } from './pages/cases/cases.component';
import { ContactComponent } from './pages/contact/contact.component';
import { NewsComponent } from './pages/news/news.component';
import { AttorneydetailsComponent } from './pages/attorneys/attorneydetails/attorneydetails.component';
import { AttorneyListComponent } from './pages/attorneys/attorney-list/attorney-list.component';
import { NewsListComponent } from './pages/news/news-list/news-list.component';
import { NewsDetailsComponent } from './pages/news/news-details/news-details.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './dashboard/login/login.component';
import { DashboardsideComponent } from './dashboard/dashboardside/dashboardside.component';
import { DashboardmainComponent } from './dashboard/dashboardmain/dashboardmain.component';
import { AdminComponent } from './dashboard/admin/admin.component';
import { ManagenewsComponent } from './dashboard/admin/managenews/managenews.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AddNewsComponent } from './dashboard/admin/managenews/add-news/add-news.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavigationComponent,
    HeaderComponent,
    PagesComponent,
    FooterComponent,
    FirmComponent,
    SidebarComponent,
    AttorneysComponent,
    ServicesComponent,
    CasesComponent,
    ContactComponent,
    NewsComponent,
    AttorneydetailsComponent,
    AttorneyListComponent,
    NewsListComponent,
    NewsDetailsComponent,
    DashboardComponent,
    LoginComponent,
    DashboardsideComponent,
    DashboardmainComponent,
    AdminComponent,
    ManagenewsComponent,
    AddNewsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    AngularEditorModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule { }
