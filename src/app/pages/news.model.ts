export default interface News {
  title: string;
  author: string;
  date: string;
  desc?: string;
  photo?: string;
}
