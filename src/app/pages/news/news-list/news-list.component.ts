import { Component, OnInit } from '@angular/core';
import { NewsService } from '../../news.service';
import { Title } from '@angular/platform-browser';
import News from '../../news.model';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss']
})
export class NewsListComponent implements OnInit {

  news: News[] = [];
  constructor(private newsService: NewsService, private titleService: Title) { }

  ngOnInit() {
    this.news = this.newsService.getNews();
  }

}
