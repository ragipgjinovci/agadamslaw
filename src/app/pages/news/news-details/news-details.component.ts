import { ActivatedRoute } from '@angular/router';
import { NewsService } from './../../news.service';
import { Component, OnInit } from '@angular/core';
import News from '../../news.model';

@Component({
  selector: 'app-news-details',
  templateUrl: './news-details.component.html',
  styleUrls: ['./news-details.component.scss']
})
export class NewsDetailsComponent implements OnInit {
  new: News;
  id: number;
  constructor(private newsService: NewsService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.new = this.newsService.getSpecificNews(this.id);
  }

}
