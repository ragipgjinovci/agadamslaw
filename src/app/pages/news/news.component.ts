import { Title } from '@angular/platform-browser';
import { NewsService } from './../news.service';
import { Component, OnInit } from '@angular/core';
import News from '../news.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  news: News[] = [];
  constructor(private newsService: NewsService, private titleService: Title) { }

  ngOnInit() {

    this.titleService.setTitle('News | AG Adams Law');
  }

}
