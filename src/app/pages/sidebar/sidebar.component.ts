import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  image: string = '../../../assets/sidebarimg.webp';
  constructor(private router: Router) { }
  url: string;
  ngOnInit() {
    this.url = this.router.url;
    if (this.url === '/page/attorneys/0') {
      this.image = '../../../assets/AGA-3-BW.webp';
    }
    else {
      this.image = '../../../assets/sidebarimg.webp';
    }
  }
  ngDoCheck() {
    this.url = this.router.url;
    if (this.url === '/page/attorneys/0') {
      this.image = '../../../assets/AGA-3-BW.webp';
    }
    else if (this.url === '/page/attorneys/1') {
      this.image = '../../../assets/AAB-BW.webp';
    }
    else {
      this.image = '../../../assets/sidebarimg.webp';
    }
  }
}
