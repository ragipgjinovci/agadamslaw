import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  panelOpenState = false;
  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle('Services | AG Adams Law');
  }

}
