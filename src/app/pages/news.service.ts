import { Injectable } from '@angular/core';
import  News from './news.model';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private news: News[] = [
    {
      title: 'AG Adams Law, P.A. Recognized in US News and World Report Best Law Firm Rankings for 2017',
      author: 'Annie Bryan',
      date: 'January 10, 2017',
      desc: 'JACKSONVILLE, FL – AG Adams Law, P.A. has been recognized as a 2017 “Best Law Firm” Metro Tier 2, Construction Law by U.S. News & World Report. Selection as a “Best Law Firm” is based on a combination of client feedback, Law Firm surveys, and the Best Lawyers peer review process. To be eligible for inclusion in the list, a law firm must have at least one attorney who received high enough peer reviews to be included in U.S. News & World Report’s annual Best Lawyers® list.AG Adams Law founder, Adam Gillespie “Lep” Adams, III said, “It’s an honor for the firm to be included in the U.S News & World Report rankings in our second year of existence. We are grateful to our peers for the nomination and to our clients for their continued trust in our work.” For view the complete list of “Best Law Firms,” see the U.S. News & World Report release here.',
    },
    {
      title: 'Adam G. “Lep” Adams III recognized as Florida Super Lawyer, 2016',
      author: 'Annie Bryan',
      date: 'November 16, 2016',
      desc: 'Adam G. “Lep” Adams, III Recognized as a “Super Lawyer” for the 10th Year. Jacksonville, FL – Adam G. “Lep” Adams, III has been recognized as a “Super Lawyer” by Super Lawyers, the Thompson Reuters rating service for the 10th year. Super Lawyers is an annual list of outstanding attorneys from more than 70 practice areas who have attained peer recognition and professional achievement.   Lawyers may be selected to the list through peer nominations, peer evaluations and independent research on the part of the Super Lawyers organization.Lep Adams said, “Inclusion in the annual Super Lawyers list is an incredible honor, as selection is based on nominations and evaluations from my peers. I’m thankful to the legal community for this recognition and their nominations for the past decade.”For more information about Super Lawyers or the selection process, visit the Super Lawyers website, here.',
      photo: '../../assets/legal_counsel_1-150x150.webp'
    },
    {
      title: 'AG Adams recognized in Best Lawyers in America list, 2016',
      author: 'Annie Bryan',
      date: 'November 16, 2016',
      photo: '../../assets/legal_counsel_2-150x150.webp'
    }
  ];

  getNews() {
    return this.news;
  }
  getSpecificNews(id) {
    return this.news[id];
  }
  constructor() { }
}
