import { Injectable } from '@angular/core';
import Attorney from './attorney.model';

@Injectable({
  providedIn: 'root'
})
export class AttorneyService {
  private attorneys: Attorney[] = [
    {
      name: 'Adam Gillespie Adams',
      image: '../../../assets/AGA-3-BW.webp',
      bio: `
     <p>Adam Gillespie “Lep” Adams, III founded AG Adams Law, P.A., and provides leadership over all practice areas. Mr. Adams is Board Certified in construction law by the Florida Bar, a recognition reserved for attorneys with the highest level of specialized skills, proficiency, professionalism and ethics as determined by the Florida Bar. In addition, he has been Peer Review Rated as AV® Preeminent™, recognized as having the highest level of professionalism and skills<br>

      Mr. Adams practice is concentrated on complex commercial litigation with particular emphasis on contract, construction and real estate disputes. Mr. Adams brings more than 30 years of experience representing and advising construction industry clients in all manner of projects, including oceanfront condominiums, hotels, government buildings and other commercial and multi-family residential developments. In addition to litigation, Mr. Adams devotes much of his practice to risk analysis and mitigation for clients, drafting and negotiating contracts to anticipate problems and minimize clients’ future exposure on projects.<br>

      Prior to founding AG Adams Law, Mr. Adams was a partner at Foley and Lardner, LLP, and a shareholder at Rogers Towers, P.A. Mr. Adams is consistently recognized in The Best Lawyers in America© list and the Florida Super Lawyers® lists for his work in construction law, and he was named Jacksonville’s Construction Lawyer of the Year in 2014. He has been recognized by the Legal 500 and he has been named one of Florida Trend’s Legal Elite™ for multiple years.<br>

      Mr. Adams resides in Jacksonville, FL with his wife, Katie. Mr. Adams resides in Jacksonville, FL with his wife, Kate and enjoys spending time with his two grandsons.<br><br><br>



      Court Admissions:
      <ul>
        <li>All Florida State Courts</li>
        <li>US District Court, Middle and Southern Districts of Florida</li>
      </ul><br><br>

      Recognitions:
      <ul>
      <li>Board Certified in construction law by the Florida Bar</li>
      <li>Peer Review Rated AV® Preeminent™ by Martin-Dale Hubbell (highest level of professional excellence and professionalism)</li>
      <li>Best Lawyers in America©, construction law (2006 – 2016)</li>
      <li>Florida Super Lawyers® (2006 – 2016) (peer review recognition for top 5% of attorneys)</li>
      <li>Jacksonville’s Construction Lawyers of the Year (2014)</li>
      <li>Legal 500, construction law (2012)</li>
      <li>Florida Trend, Florida Legal Elite™ (2008, 2010, 2012)</li>
      </ul><br><br>
      Professional Memberships:
      <ul>
      <li>American Bar Association, Forum on Construction Industry and Torts Insurance Practice Section</li>
      <li>The Florida Bar Association</li>
      <li>The Jacksonville Bar Association</li>
      </ul><br><br>

      Professional Affiliations and Leadership:
      <ul>
      <li>The Florida Bar, Board of Governors (1996 – 1998)</li>
      <li>The Florida Bar, Young Lawyers Division, Past President and member (1993 – 1998)</li>
      <li>Fourth Judicial Circuit Professionalism Committee, Co-chair (1998 – 2004)</li>
      <li>The Florida Bar Lawyer Referral Service Committee, Past Chair and member (1997 – 2004)</li>
      <li>Fourth Judicial Circuit Grievance Committee, Past Chair (1992 – 1995; 2001 – 2004)</li>
      <li>The Jacksonville Bar Association, Board of Governors (1998 – 2001)</li>
      <li>Barrister of the Chester Beddell Inn or Court</li>
      <li>Florida Supreme Court Commission on Professionalism, Member (1996 – 2000)</li>
      </ul><br><br>

      Thought Leadership:
      <ul>
      <li>Co-author, “Chapter 6 Rights and Liabilities of Subcontractors and Suppliers,” Florida Construction Law and Practice (5th, 6th, 7th, and 8th Editions), published by the Florida Bar</li>
      <li>Lecturer on real property and related topics, including real property title disputes, construction lien and bond claims, payment disputes and contract termination and changes</li>
      <li>Construction Super Conference Presenter</li>
      </ul><br><br>
      Education:
      <ul>
      <li>The University of Florida, Levin College of Law, J.D. (1983)</li>
      <li>The University of Florida, B.A. (1980)</li></ul></p><br><br><br>`
    },
    {
      name: 'Ann Adams Bryan',
      image: '../../../assets/AAB-BW.webp',
      bio: `
     <p>Ann “Annie” Adams Bryan joined AG Adams Law in 2016. Her practice is concentrated in complex commercial and construction litigation.

     Ms. Bryan is a graduate of the University of North Carolina at Chapel Hill’s School of Law, where she was honored as a member of the Davis Society and the recipient of the Winston Crisp Student Leadership Award. She spent a summer interning in the United States District Court for the Honorable Marcia Morales Howard, and she completed more than 100 hours of pro bono services during her time as a student. She received a Bachelor of Science from the University of Florida, where she was recognized as the Most Outstanding Student Leader and gave the University’s commencement address.<br>

     Ms. Bryan resides in Jacksonville, FL with her husband, Josh and their son James. She enjoys serving in community roles that benefit women and children, including being a Women’s Giving Alliance Delores Barr Weaver Fellow, a volunteer for the Women’s Board, and a member of the community advisory council for The Birth and Newborn Center at Wolfson Children’s Hospital.<br><br>

     Recognitions
     <ul>
     <li>Davis Society, University of North Carolina Law (2013) (recognizing academic and personal excellence and willingness to serve for the betterment of the School of Law, its faculty and students)</li>
     <li>Winston Crisp Award for Student Leadership, University of North Carolina Law (2013) (Student Bar Association award recognizing excellence in student leadership)</li>
     <li>Florida Blue Key, member (inducted 2006)</li>
     <li>Most Outstanding Student Leader, University of Florida (2007)</li>
     <li>Commencement Speaker, University of Florida (2007)</li>
     </ul><br><br>
     Professional Membership:
     <ul>
     <li>The Florida Bar Association</li>
     <li>The Jacksonville Bar Association</li>
     </ul><br><br>
     Community Involvement:
     <ul>
     <li>Women’s Giving Alliance, Delores Barr Weaver Fellow, Class of 2016</li>
     <li>The Women’s Board, Blog and Social Media Committee, Chair and member (2014 – 2016)</li>
     <li>Birth and Newborn Advisory Council, Wolfson Children’s Hospital (2016)</li>
     </ul><br><br>
     Education:
     <ul>
     <li>The University of North Carolina, Chapel Hill, College of Law, J.D. (2013)
     <li>The University of Florida, B.S. (2007)</ul></p><br><br>`
    }
  ];
  constructor() { }
  getAttorneys() {
    return this.attorneys;
  }
  getAttorney(id: number) {
    return this.attorneys[id];
  }
}
