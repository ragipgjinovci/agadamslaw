export default interface Attorney {
  name: string;
  image: string;
  bio: string;
}
