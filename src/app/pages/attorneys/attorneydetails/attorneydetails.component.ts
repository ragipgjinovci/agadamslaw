import { AttorneyService } from './../attorney.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import Attorney from '../attorney.model';

@Component({
  selector: 'app-attorneydetails',
  templateUrl: './attorneydetails.component.html',
  styleUrls: ['./attorneydetails.component.scss']
})
export class AttorneydetailsComponent implements OnInit {
  attorney: Attorney;
  id: number;
  constructor(private attorneysService: AttorneyService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.attorney = this.attorneysService.getAttorney(this.id);
  }

}
