import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AttorneyService } from '../attorney.service';
import Attorney from '../attorney.model';

@Component({
  selector: 'app-attorney-list',
  templateUrl: './attorney-list.component.html',
  styleUrls: ['./attorney-list.component.scss']
})
export class AttorneyListComponent implements OnInit {

  attorneys: Attorney[] = [];
  constructor(private titleService: Title, private attorneysService: AttorneyService) { }

  ngOnInit() {
    this.titleService.setTitle('Attorneys | AG Adams Law');
    this.attorneys = this.attorneysService.getAttorneys();
  }
  shorten(str, maxLen, separator = '.') {
    return str.substr(0, str.lastIndexOf(separator, maxLen));
  }

}
